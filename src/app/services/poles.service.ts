import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../environments/environment';
import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PolesService {

  constructor(private httpClient: HttpClient) { }

  getPoles(municipalityName: string): Observable<any> {
    return this.httpClient.get(`${environment.json_server}/pali?comune=${municipalityName}`).pipe(tap(async (res: any) => {}));
  }
}
