import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root',
})
export class ReportsService {
  constructor(private httpClient: HttpClient) {}

  getReports(): Observable<any> {
    return this.httpClient.get(`${environment.host}/api/sensor/posts`).pipe(tap(async (res: any) => {}));
  }

  getMyPosts(): Observable<any> {
    return this.httpClient
      .get(`${environment.host}/api/sensor/users/current/posts?sortField=published_at&sortDirection=desc`)
      .pipe(tap(async (res: any) => {}));
  }

  getReportById(postId: string): Observable<any> {
    return this.httpClient
      .get(`${environment.host}/api/sensor/posts/${postId}/comments`)
      .pipe(tap(async (res: any) => {}));
  }

  getReportsByUrl(url: string): Observable<any> {
    return this.httpClient.get(url).pipe(tap(async (res: any) => {}));
  }


  createPublicPost(data: any, polesData: any): Observable<any> {

    let sensor_data = {
      subject: data.type + ' ID: #'  +  polesData.id + ' Quadro: '+ polesData.fkquadro,
      is_private: false,
      description: data.description,
      channel: 'App mobile',
      category: 107, // Illuminazione pubblica
      type: data.type,
      ...(data.geocode &&
        data.geocode.length > 0 && {
          address: {
            address: data.address,
            longitude: data.geocode[1],
            latitude: data.geocode[0],
          },
        }),
      ...(data.images &&
        data.images.length > 0 && {
          images: data.images,
        }),
    };

    return this.httpClient
      .post(`${environment.host}/api/sensor/posts`, sensor_data)
      .pipe(tap(async (res: any) => {}));
  }

  createCommentPost(data: any, postId: string): Observable<any> {
    return this.httpClient
      .post(`${environment.host}/api/sensor/posts/${postId}/comments`, data)
      .pipe(tap(async (res: any) => {}));
  }
}
