import { Component, ElementRef, NgZone, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { icon, LatLngExpression, Map, Marker, tileLayer, marker, PopupOptions, DomEvent, DomUtil } from 'leaflet';
import { Apollo } from 'apollo-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { ZoneService } from '../../services/zone.service';
import { Geolocation, Position } from '@capacitor/geolocation';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PolesService } from '../../services/poles.service';
import { StorageService } from '../../services/storage.service';
import { Router } from '@angular/router';
import * as L from 'leaflet';
import 'leaflet.markercluster';
import { Control } from 'leaflet';

@Component({
  selector: 'app-eco-points-details',
  templateUrl: './poles-map.page.html',
  styleUrls: ['./poles-map.page.scss'],
})

export class PolesMapPage implements OnInit, OnDestroy {
  error: any;
  label = 'Seleziona elemento sulla mappa';
  position: Position = null;
  currentPos: GeolocationPosition;
  waitingForCurrentPosition = false;
  @ViewChild('map', { read: ElementRef, static: true }) mapElement: ElementRef;
  @ViewChild('one', { static: false }) d1: ElementRef;
  municipalityData: any;
  private content: any;
  private geocode: LatLngExpression = [0, 0];
  private marker: Marker;
  private mapRef: Map;
  private unsubscribe$ = new Subject<void>();
  private positionMarkersArray: any[] = [];
  private token: string;
  private sizePoles: any;

  constructor(
    private apollo: Apollo,
    public sanitizer: DomSanitizer,
    public modalController: ModalController,
    public ngZone: NgZone,
    private zoneService: ZoneService,
    private polesService: PolesService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    private storage: StorageService,
    private router: Router
  ) {}

  ngOnInit() {
    this.storage.getToken().then((res) => {
      if (res) {
        this.token = JSON.parse(res).token;
      } else {
        this.token = null;
      }
    });

    this.storage.getMunicipality().then((res) => {
      this.municipalityData = JSON.parse(res) || null;

      this.polesService
        .getPoles(this.municipalityData.comune)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (result) => {
            this.content = result;
            this.sizePoles = result.length
            if (this.mapRef !== undefined) { this.mapRef.remove(); }
            this.initMap(this.sizePoles);
          },
          (error) => {
            this.error = 'Nessun elemento trovato per questo comune';
          }
        );
    });
  }

  ionViewWillEnter() {
    this.storage.getToken().then((res) => {
      if (res) {
        this.token = JSON.parse(res).token;
      } else {
        this.token = null;
      }
    });

    this.storage.getMunicipality().then((res) => {
      this.municipalityData = JSON.parse(res) || null;
      this.polesService
        .getPoles(this.municipalityData.comune)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (result) => {
            this.content = result;
            this.sizePoles = result.length
            if (this.mapRef !== undefined) { this.mapRef.remove(); }
            this.initMap(this.sizePoles);
          },
          (error) => {
            this.error = 'Nessun elemento trovato per questo comune';
          }
        );
    });
  }

  initMap(sizePoles: any) {
    this.mapRef= new Map(this.mapElement.nativeElement, { zoomControl: false, tap: false  }).setView(this.geocode, 10);
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(this.mapRef);

    L.control.zoom({ position: 'topright' }).addTo(this.mapRef);

    const customMarkerIcon = icon({
      iconUrl: 'assets/icon/lamp.svg',
      iconSize: [80, 100],
      popupAnchor: [0, -40],
    });

    const currentPositionMarkerIcon = icon({
      iconUrl: 'assets/icon/current-location.svg',
      iconSize: [20, 40],
      popupAnchor: [0, -20],
    });

    this.marker = marker(this.geocode, { icon: currentPositionMarkerIcon, draggable: false,bubblingMouseEvents:false }).addTo(this.mapRef);

    if (this.content && this.content.length > 0) {
      const markerCluster = L.markerClusterGroup({
        spiderfyOnMaxZoom: true,
        showCoverageOnHover: true,
        zoomToBoundsOnClick: true,
        iconCreateFunction(cluster) {
          const childCount = cluster.getChildCount();
          return new L.DivIcon({
            html: '<div class="marker"><span>' + childCount + '</span></div>',
            className: 'marker-cluster-small',
            iconSize: new L.Point(40, 40),
          });
        },
      });

      if (this.marker) {
        markerCluster.addLayer(this.marker).addTo(this.mapRef);
      }
      this.content.forEach((el) => {
        const loc = el.point;
        // create popup contents
        let customPopup = `
<div><h4>Descrizione</h4><div><p>
<strong>Quadro</strong>: ${el.fkquadro}<br>
<strong>Supporto fissaggio</strong>: <br>
<strong>Tipo di apparato</strong>: ${el.tapp1}<br>
<strong>Tipo di lampada</strong>: ${el.tlamp1}<br>
<strong>Potenza lampada</strong>: ${el.wlamp1} watt<br/>
<strong>Ubicazione impianto</strong>:
Stradale</p><h4>Sostegno</h4><p>
<strong>Materiale</strong>: ${el.sost_mat}<br>
<strong>Finiture</strong>: ${el.sost_fin}<br>
<strong>Tipo di sostegno</strong>: ${el.sost_tipo}<br><strong>Altezza</strong>: ${el.altezza} metri</p>
</div></div>`;

        if (this.token) {
          const ctaButton =
            '<ion-button color="primary" class="d-block" id="' +
            loc.toString() +
            '">\n' +
            '  <ion-label>\n' +
            '    Invia segnalzione\n' +
            '  </ion-label>\n' +
            '</ion-button>';
          customPopup += ctaButton;
        }

        // specify popup options
        const customOptions: PopupOptions = {
          maxWidth: 500,
          maxHeight: 400,
          className: 'layer-popup',
        };

        markerCluster.addLayer(
          L.marker(loc, { icon: customMarkerIcon,bubblingMouseEvents:false })
            .togglePopup()
            .bindPopup(customPopup, customOptions)
            .on('click', (event) => {
              const position = event.target.getLatLng();
              const positionStringify = loc.toString();
              const data = this.searchDataMarker(positionStringify)[0] || null;
              const buttonSubmit = DomUtil.get(positionStringify);
              if (buttonSubmit) {
                L.DomEvent.addListener(buttonSubmit, 'click', (ee) => {
                  DomEvent.stopPropagation(ee);
                  this.router.navigate(['new-post-elements'], { state: { dataDetails: data, address: this.label } });
                });
              }
              this.updateLabelMarkerStatic(position);
            })
        )
        this.positionMarkersArray.push(loc);
      });
      this.mapRef.addLayer(markerCluster);
      this.mapRef.fitBounds(this.positionMarkersArray);
    }

    const legend = new Control({position: 'topleft'});

    legend.onAdd = (map) =>  {
      const div = L.DomUtil.create('div', 'info legend');
      div.innerHTML += `<b>N° pali: ${sizePoles || null}</b>`;
      return div;
    };

    legend.addTo(this.mapRef);

  }

  searchDataMarker(searchData: string) {
    return this.content.filter((el) => el.point.toString() === searchData);
  }

  centerLeafletMapOnMarker(map, marker) {
    const latLngs = [marker.getLatLng()];
    const markerBounds = L.latLngBounds(latLngs);
    map.fitBounds(markerBounds);
  }

  updateLabelMarkerStatic(position: any) {
    this.zoneService.addressLookup([position.lat, position.lng]).subscribe((res) => {
      const data = res.replace(/cb\(/g, '');
      const jsonData = JSON.parse(data.replace(/\)/g, ''));
      this.label = jsonData.display_name;
    });
  }

  updateLabelMarker(position: any) {
    this.zoneService.addressLookup([position.lat, position.lng]).subscribe((res) => {
      const data = res.replace(/cb\(/g, '');
      const jsonData = JSON.parse(data.replace(/\)/g, ''));
      this.label = jsonData.display_name;
      this.geocode = [position.lat, position.lng];
      this.marker.setPopupContent(`<b>${jsonData.display_name}</b>`).openPopup();
    });
  }

  async requestPermissions() {
    const permResult = await Geolocation.requestPermissions();
    console.log('Perm request result: ', permResult);
  }

  async getCurrentPos() {
    try {
      this.waitingForCurrentPosition = true;
      const coordinates = await Geolocation.getCurrentPosition();
      console.log('coordinates', coordinates);
      this.position = coordinates;

      this.zoneService
        .addressLookup([this.position.coords.latitude, this.currentPos.coords.longitude])
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (res) => {
            console.log('res', res);
            const data = res.replace(/cb\(/g, '');
            const jsonData = JSON.parse(data.replace(/\)/g, ''));
            this.label = jsonData.display_name;
            this.geocode = [this.position.coords.latitude, this.position.coords.longitude];
            this.addCurrentMakerPosition();
          },
          (error) => {
            console.log('err', error);
          },
          () => {
            console.log('finish');
          }
        );
    } catch (err) {
      console.error('Failed to get current position.', err);
    } finally {
      this.waitingForCurrentPosition = false;
    }
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Caricamento...',
    });
    await loading.present().then(() => {
      Geolocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
      })
        .then((r) => {
          this.zoneService.addressLookup([r.coords.latitude, r.coords.longitude]).subscribe(
            (res) => {
              const data = res.replace(/cb\(/g, '');
              const jsonData = JSON.parse(data.replace(/\)/g, ''));
              this.label = jsonData.display_name;
              this.geocode = [r.coords.latitude, r.coords.longitude];
              this.addCurrentMakerPosition();
              loading.dismiss();
            },
            (error) => {
              loading.dismiss();
            },
            () => {
              loading.dismiss();
            }
          );
          loading.dismiss();
        })
        .catch((err) => {
          loading.dismiss();
          this.presentToastWithOptions();
        });
    });

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Geocalizazzione non abilitata',
      message: '',
      position: 'top',
      buttons: [
        {
          text: 'Chiudi',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    await toast.present();

    const { role } = await toast.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  ngOnDestroy(): void {
    // destroy instance map
    setTimeout(_=>{
      this.mapRef.off();
      this.mapRef.remove();
    }, 200)

    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  addCurrentMakerPosition() {
    const currentPositionMarkerIcon = icon({
      iconUrl: 'assets/icon/current-location.svg',
      iconSize: [20, 40],
      popupAnchor: [0, -20],
    });
    if (this.geocode[0] !== 0) {
      this.positionMarkersArray.push(this.geocode);
      this.marker = marker(this.geocode, { icon: currentPositionMarkerIcon, draggable: false })
        .bindPopup(`<b>${this.label}</b>`, { autoClose: false })
        .on('click', () => {})
        .on('dragend', (event) => {
          const marker = event.target;
          const position = marker.getLatLng();
          this.updateLabelMarker(position);
        })
        .addTo(this.mapRef);
    }

    const customMarkerIcon = icon({
      iconUrl: 'assets/icon/lamp.svg',
      iconSize: [40, 102],
      popupAnchor: [0, -40],
    });

    if (this.content && this.content.length > 0) {
      const markerCluster = L.markerClusterGroup({
        spiderfyOnMaxZoom: true,
        showCoverageOnHover: true,
        zoomToBoundsOnClick: true,
        iconCreateFunction(cluster) {
          const childCount = cluster.getChildCount();
          return new L.DivIcon({
            html: '<div class="marker"><span>' + childCount + '</span></div>',
            className: 'marker-cluster-small',
            iconSize: new L.Point(40, 40),
          });
        },
      });

      if (this.marker) {
        markerCluster.addLayer(this.marker);
      }
      this.content.forEach((el) => {
        const loc = el.location.substring(1).slice(0, -1).split(',');
        markerCluster.addLayer(L.marker(loc, { icon: customMarkerIcon }).bindPopup(el.name));
        this.positionMarkersArray.push(loc);
      });
      this.mapRef.addLayer(markerCluster);
      this.mapRef.fitBounds(this.positionMarkersArray);
    }
  }
}
