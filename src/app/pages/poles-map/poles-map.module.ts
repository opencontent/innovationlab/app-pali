import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EcoPointsDetailsPageRoutingModule } from './poles-map-routing.module';

import { PolesMapPage } from './poles-map.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, EcoPointsDetailsPageRoutingModule],
  declarations: [PolesMapPage],
})
export class EcoPointsDetailsPageModule {}
