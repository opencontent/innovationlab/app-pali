import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MunicipalityListPage } from './municipality-list.page';

const routes: Routes = [
  {
    path: '',
    component: MunicipalityListPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MunicipalityListPageRoutingModule {}
