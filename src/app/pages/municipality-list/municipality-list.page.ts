import { Component, OnDestroy, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { StorageService } from '../../services/storage.service';
import { Output, EventEmitter } from '@angular/core';
import { ZoneService } from '../../services/zone.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { PolesService } from '../../services/poles.service';

@Component({
  selector: 'app-municipality-list',
  templateUrl: './municipality-list.page.html',
  styleUrls: ['./municipality-list.page.scss'],
})
export class MunicipalityListPage implements OnInit, OnDestroy {
  comuni: any[];
  loading = true;
  error: any;
  selectedMunicipality: any;
  private unsubscribe$ = new Subject<void>();


  @Output() newItemEvent = new EventEmitter<string>();
  private size: any;

  constructor(
    private apollo: Apollo,
    public navCtrl: NavController,
    private router: Router,
    private storage: StorageService,
    private zoneService: ZoneService,
    private polesService: PolesService
  ) {}

  ngOnInit() {
    this.storage.getMunicipality().then((res) => {
      this.selectedMunicipality = JSON.parse(res);

      this.polesService
        .getPoles(this.selectedMunicipality.comune)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (result) => {
            this.size = result.length
          },
          (error) => {
            this.error = 'Nessun elemento trovato per questo comune';
          }
        );

      this.zoneService.getMunicipalities()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(res =>{
          this.comuni = res
        })
    });
  }

  async selectMunicipality(data) {
    this.storage
      .set(
        'municipality',
        JSON.stringify({
          id: data.id,
          comune: data.comune,
          nomecomune: data.nomecomune,
          size: this.size
        })
      )
      .then(() => {
        this.selectedMunicipality = data;
        this.updateMunicipality(this.selectedMunicipality);
        setTimeout(() => {
          this.router.navigate(['/home']);
        },500)

      });
  }



  ionViewWillEnter() {
    this.storage.getMunicipality().then((res) => {
      this.selectedMunicipality = JSON.parse(res);
      this.polesService
        .getPoles(this.selectedMunicipality.comune)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (result) => {
            this.size = result.length
          },
          (error) => {
            this.error = 'Nessun elemento trovato per questo comune';
          }
        );

      this.zoneService.getMunicipalities()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(res =>{
          this.comuni = res
      })
    });
  }

  updateMunicipality(value: any) {
    this.newItemEvent.emit(value);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
