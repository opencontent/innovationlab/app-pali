import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardGuard } from './auth-guard.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'municipality-list',
    loadChildren: () =>
      import('./pages/municipality-list/municipality-list.module').then((m) => m.MunicipalityListPageModule),
  },
  {
    path: 'select-municipality',
    loadChildren: () =>
      import('./modals/select-municipality/select-municipality.module').then((m) => m.SelectMunicipalityPageModule),
  },
  {
    path: 'municipalities',
    loadChildren: () => import('./modals/municipalities/municipalities.module').then((m) => m.MunicipalitiesPageModule),
  },
  {
    path: 'posts',
    loadChildren: () => import('./pages/posts/posts.module').then((m) => m.PostsPageModule),
    canActivate: [AuthGuardGuard],
  },
  {
    path: 'new-post-elements',
    loadChildren: () => import('./pages/new-post-elements/new-post-elements.module').then((m) => m.NewPostElementsPageModule),
    canActivate: [AuthGuardGuard],
  },
  {
    path: 'post/:id',
    loadChildren: () => import('./pages/post-details/post-details.module').then((m) => m.PostDetailsPageModule),
    canActivate: [AuthGuardGuard],
  },
  {
    path: 'map',
    loadChildren: () => import('./modals/map/map.module').then((m) => m.MapPageModule),
  },
  {
    path: 'poles-map',
    loadChildren: () =>
      import('./pages/poles-map/poles-map.module').then((m) => m.EcoPointsDetailsPageModule),
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then((m) => m.SettingsPageModule),
  },
  {
    path: 'privacy',
    loadChildren: () => import('./pages/privacy/privacy.module').then((m) => m.PrivacyPageModule),
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then((m) => m.ProfilePageModule),
    canActivate: [AuthGuardGuard],
  },
  {
    path: 'fullscreen-image',
    loadChildren: () => import('./modals/fullscreen-image/fullscreen-image.module').then( m => m.FullscreenImagePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      relativeLinkResolution: 'legacy',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
